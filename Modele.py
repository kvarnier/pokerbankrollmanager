import sqlite3

class Database(object):
    

    def connect(self) : 
        self.databaseFile = "C:/Users/kevin/source/PokerBankrollManager/bdd.sq3"
        self.connection = sqlite3.connect(self.databaseFile)
        self.cursor = self.connection.cursor()

    def addTableUsers(self) :    
        self.cursor.execute("create table users(email text, password text, bankroll float)")
    
    def dropTable(self,tablename):
        sqlQuery = "drop table " + tablename
        self.cursor.execute(sqlQuery)

    def insertTableUsers(self, email, password, brAmount = 0):
        
        sqlQuery = "insert into users(email,password,bankroll) values ('"+email+"','"+password+"','"+brAmount+"')"
        self.cursor.execute(sqlQuery)


    def commit(self):
        self.connection.commit()

    def execute(self):
        self.cursor.execute("select * from  users")
        usersData = self.cursor.fetchall()
        return usersData

    def testRowid(self): 
        self.cursor.execute("select rowid from users")
        rowid= self.cursor.fetchall()
        return rowid

    def close(self):  
        self.cursor.close()
        self.connection.close()   
          
        

           


