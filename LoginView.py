from tkinter import * 
from Controller import *

class LoginView(Login):
    def __init__(self):
        self.LoginWindows = Tk()
        self.LoginWindows.geometry("300x180+750+300")
        self.LoginWindows.title("Login")
        self.LoginWindows.iconbitmap('favicon.ico')

        self.LoginWindows.resizable(width=False,height=False)

        self.EmailLabel = Label(self.LoginWindows,text="Email")
        self.EmailLabel.grid(row = 0, column =0 )
        self.PasswordLabel = Label(self.LoginWindows,text="Password")
        self.PasswordLabel.grid(row = 1, column =0, padx =10)

        self.EmailEntry = Entry(self.LoginWindows,width= 30)
        self.EmailEntry.grid(row = 0, column =1, pady = 8)

        self.PasswordEntry = Entry(self.LoginWindows,show="*",width= 30)
        self.PasswordEntry.grid(row = 1, column =1)

        self.ValidationButton = Button(self.LoginWindows, text="Validate", command = self.validate)
        self.ValidationButton.grid (row = 2, column =1, pady = 3, padx = 0)

        self.RegistrationButton = Button(self.LoginWindows, text="Register", command = self.pushRegisterButton)
        self.RegistrationButton.grid (row = 3, column =1, pady = 3, padx = 0)

        self.QuitButton = Button(self.LoginWindows, text="Quit", command = self.LoginWindows.destroy)
        self.QuitButton.grid (row = 4, column =1, pady = 3)
       
        self.LoginWindows.mainloop()
    
    def returnErrorLogin(self):
        self.ErrorLabel = Label(self.LoginWindows,text="Identification error")
        self.ErrorLabel.grid(row = 5, column =1, padx =10)

   