from tkinter import * 
from Controller import *



class RegisterView(object):
    def __init__(self):
        self.RegisterWindows = Tk()
        self.RegisterWindows.geometry("350x150+400+150")
        self.RegisterWindows.title("Register")
        self.RegisterWindows.iconbitmap('favicon.ico')
        self.RegisterWindows.resizable(width=False,height=False)

        self.EmailLabel = Label(self.RegisterWindows,text="Email")
        self.EmailLabel.grid(row = 0, column =0 )

        self.PasswordLabel = Label(self.RegisterWindows,text="Password")
        self.PasswordLabel.grid(row = 1, column =0, padx =10)

        self.ConfirmPasswordLabel = Label(self.RegisterWindows,text=" Confirm Password")
        self.ConfirmPasswordLabel.grid(row = 2, column =0, padx =10)

        self.EmailEntry = Entry(self.RegisterWindows,width= 30)
        self.EmailEntry.grid(row = 0, column =1, pady = 8)

        self.PasswordEntry = Entry(self.RegisterWindows,show="*",width= 30)
        self.PasswordEntry.grid(row = 1, column =1, pady = 8)

        self.ConfirmPasswordEntry = Entry(self.RegisterWindows,show="*",width= 30)
        self.ConfirmPasswordEntry .grid(row = 2, column =1, pady = 8)

        self.ValidationButton = Button(self.RegisterWindows, text="Validate")
        self.ValidationButton.grid (row = 3, column =1, pady = 3, padx = 0)

        self.RegisterWindows.mainloop()
    
        
     
    
    
   